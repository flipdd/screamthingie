// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickableActor.h"
#include "KnifeActor.generated.h"

/**
 * 
 */
UCLASS()
class SCREAMTHING_API AKnifeActor : public APickableActor
{
	GENERATED_BODY()

public:
	virtual void PickUp(class USceneComponent* Mesh, class APickableActor*& PickableActor);

	virtual void Release();
};
