// Fill out your copyright notice in the Description page of Project Settings.


#include "KnifeActor.h"
#include "Components/StaticMeshComponent.h" 

void AKnifeActor::PickUp(USceneComponent* Mesh, APickableActor*& PickableActor)
{
	//CapsuleComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	AttachToComponent(Mesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, FName("PickableSocket"));
	auto Pickable = Cast<APickableActor>(this);
	PickableActor = Pickable;
}

void AKnifeActor::Release()
{
	DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}
