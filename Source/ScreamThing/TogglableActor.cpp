// Fill out your copyright notice in the Description page of Project Settings.


#include "TogglableActor.h"
#include "Components/CapsuleComponent.h" 
#include "Components/SkeletalMeshComponent.h"

// Sets default values
ATogglableActor::ATogglableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>("CapsuleComponent");
	CapsuleComponent->SetupAttachment(GetRootComponent());

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	SkeletalMesh->SetupAttachment(CapsuleComponent);
}

// Called when the game starts or when spawned
void ATogglableActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATogglableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

