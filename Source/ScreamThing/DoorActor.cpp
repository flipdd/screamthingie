// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorActor.h"

void ADoorActor::Toggle()
{
	if (bIsOpened)
		SkeletalMesh->SetPlayRate(-1);
	else
		SkeletalMesh->SetPlayRate(1);

	SkeletalMesh->Play(false);
	bIsOpened = !bIsOpened;
}