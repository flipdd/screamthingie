// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TogglableActor.h"
#include "DoorActor.generated.h"

/**
 * 
 */
UCLASS()
class SCREAMTHING_API ADoorActor : public ATogglableActor
{
	GENERATED_BODY()
	
private:
	virtual void Toggle();
	
	bool bIsOpened = false;
};
