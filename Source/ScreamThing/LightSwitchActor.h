// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TogglableActor.h"
#include "LightSwitchActor.generated.h"

UCLASS()
class SCREAMTHING_API ALightSwitchActor : public ATogglableActor
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
		class USpotLightComponent* SpotLight;

private:
	ALightSwitchActor();

	virtual void Toggle();
};
