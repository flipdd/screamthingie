// Fill out your copyright notice in the Description page of Project Settings.


#include "LightSwitchActor.h"
#include "Components/SpotLightComponent.h"

ALightSwitchActor::ALightSwitchActor()
{
	SpotLight = CreateDefaultSubobject<USpotLightComponent>("SpotLight");
	SpotLight->SetupAttachment(RootComponent);
}

void ALightSwitchActor::Toggle()
{
	SpotLight->ToggleVisibility();

	if (SpotLight->IsVisible())
		SkeletalMesh->SetPlayRate(1);
	else
		SkeletalMesh->SetPlayRate(-1);

	SkeletalMesh->Play(false);
}
