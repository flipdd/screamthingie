// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickableActor.generated.h"

UCLASS()
class SCREAMTHING_API APickableActor : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMesh;
	
//public:	
//	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
//		class UCapsuleComponent* CapsuleComponent;
	// Sets default values for this actor's properties
	APickableActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual void PickUp(class USceneComponent* Mesh, class APickableActor*& PickableActor) PURE_VIRTUAL(ATogglableActor::CanRedo, );

	virtual void Release() PURE_VIRTUAL(ATogglableActor::CanRedo, );
};
